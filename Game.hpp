//
// Created by Keen on 18.06.2018.
//

#ifndef UNTITLED_GAME_HPP
#define UNTITLED_GAME_HPP

#include "Tetromino.hpp"
#include "Grid.hpp"
#include "Highscore.hpp"
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
class Game {
friend class Grid;
public:
    Game();
    void run();


private:
    void proceed(Direction dir);
    void update(const sf::Time& dt);
    void rotate();
    void createTetromino();
    bool isValidMovement(std::array<sf::Vector2i, 4> block);
    bool isOccupied(int x, int y);
    void processEvents();
    void render();


    sf::RenderWindow                            mRenderWindow;
    sf::Texture                                 mTexture;
    sf::RectangleShape                          mSeparationLine;
    std::unique_ptr<Tetromino>                  mTetromino;
    std::unique_ptr<Tetromino>                  mPreview;
    std::unique_ptr<Grid>                       mGrid;
    Highscore                                   mHighScore;
    sf::Time                                    mElapsedTime;
    int                                         mID;
};


#endif //UNTITLED_GAME_HPP
